<?php

namespace app\models;

use Yii;
use yii\base\Model;
use peterkahl\locale\locale as LocaleGetCode;
use Giggsey\Locale\Locale as LocaleGetCountry;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $firstname;
    public $lastname;
    public $phone;
    public $email;
    public $country_code;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['firstname', 'trim'],
            ['firstname', 'required'],
            ['firstname', 'string', 'min' => 2, 'max' => 255],
            ['lastname', 'trim'],
            ['lastname', 'string', 'min' => 2, 'max' => 255],
            ['phone', 'string'],
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator'],
            ['country_code', 'string', 'min' => 2],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\Lead', 'message' => 'Такой email адрес уже существует.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return Lead|null the saved model or null if saving fails
     */
    public function signup()
    {   
        if (!$this->validate()) {
            return null;
        }

        $lead = new Lead();
        $lead->firstname = $this->firstname;
        $lead->lastname = $this->lastname;
        $lead->email = $this->email;
        $lead->phone = $this->phone;
        $lead->lang = 'Russian';//$lead->lang = mb_strtolower($this->country_code);
        $fullCoutryCode = LocaleGetCode::country2locale($this->country_code);
        $lead->country = LocaleGetCountry::getDisplayRegion($fullCoutryCode, 'en');
        $lead->address = null;
        $lead->promo_code = '097';
        return $lead->save() ? $lead : null;
    }

}