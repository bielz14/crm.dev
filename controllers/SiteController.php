<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\Lead;
//use peterkahl\locale\locale as LocaleGetCode;
//use Giggsey\Locale\Locale as LocaleGetCountry;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'signupapi', 'signupapi'],
                'rules' => [
                    [
                        'actions' => ['signup', 'signupapi'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'signup' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        if ($session->get('model')) {
            $model = $session->get('model');
            $session->destroy();
        } else {
            $model = new SignupForm();
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {   
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $modelSignup = new SignupForm();
        if ($modelSignup->load(Yii::$app->request->post(), '')) {
            if ($user = $modelSignup->signup()) {
                return $this->render('thankyou', ['phone' => $user->phone]);
            }
        }

        $session = Yii::$app->session;
        $session->open();
        $session->set('model', $modelSignup);
        return $this->goHome();
    }

    public function actionSignupapi()
    {   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $email = Yii::$app->request->get('email');
        curl_setopt($ch, CURLOPT_URL,'https://crm.gocapitalfx.com/api/check-email');
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    'email=' . $email);
        $result = json_decode(curl_exec($ch));
        $phone = Yii::$app->request->get('phone');
        if (!$result->exist) {
            curl_setopt($ch, CURLOPT_URL,'https://crm.gocapitalfx.com/api/check-phone');
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                'phone_number=' . $phone);
            $result = json_decode(curl_exec($ch));
            if (!$result->exist) {
                $lang = 'Russian';//$lang = mb_strtolower(Yii::$app->request->get('country_code'));
                $firstname = Yii::$app->request->get('firstname');
                $lastname = Yii::$app->request->get('lastname');
                //$fullCoutryCode = LocaleGetCode::country2locale(Yii::$app->request->get('country_code'));
                $country = 'Russia';//LocaleGetCountry::getDisplayRegion($fullCoutryCode, 'en');
                $address = null;
                $promoCode = '097';
                $token = '79589170b8751e9825e912686cdaa776';
                curl_setopt($ch, CURLOPT_URL,'https://crm.gocapitalfx.com/api/v2/lead');
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                    'phone_number=+' . $phone
                    . '&language=' . $lang
                    . '&first_name=' . $firstname
                    . '&last_name=' . $lastname
                    . '&country=' . $country
                    . '&email=' . $email
                    . '&address=' . $address
                    . '&promo_code=' . $promoCode
                    . '&token=' . $token
                    . '&referer=Geralead');
                $result = json_decode(curl_exec($ch));
                if ($result->message === 'OK') {
                    curl_close ($ch);
                    return Yii::$app->response->redirect($result->autoLoginLink);
                }
            }
        }
        curl_close($ch);
        return $this->goHome();
    }
}
