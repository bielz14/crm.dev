<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lead}}`.
 */
class m190708_114839_create_lead_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lead}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'lang' => $this->string()->notNull(),
            'country' => $this->string()->notNull(),
            'address' => $this->string(),
            'promo_code' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_at_utc' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lead}}');
    }
}
