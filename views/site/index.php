<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
<div class="wrapper">
    <header class="header">
        <div class="container">
            <div class="header__cont">
                <h1 class="logo"><img src="/img/logo.png" alt=""></h1>

                <div class="social-icons">
                    <img src="/img/icon-vb.png" alt="" class="social-icons__item">
                    <img src="/img/icon-wp.png" alt="" class="social-icons__item">
                    <img src="/img/icon-tg.png" alt="" class="social-icons__item">
                    <img src="/img/icon-fb.png" alt="" class="social-icons__item">
                    <img src="/img/icon-vk.png" alt="" class="social-icons__item">
                </div>

                <div class="register-now">
                    <div class="register-now__label">сейчас проходят <br> регистрацию</div>
                    <div class="register-now__count">
                        <div class="register-now__count-number">167</div>
                        <div class="register-now__count-unit">человек</div>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </header><!-- .header -->
    <style>
        .send-form {
            background: #ff9d02;
            background: -moz-linear-gradient(top, #ff9d02 0%, #ff6d00 100%);
            background: -webkit-linear-gradient(top, #ff9d02 0%,#ff6d00 100%);
            background: linear-gradient(to bottom, #ff9d02 0%,#ff6d00 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff9d02', endColorstr='#ff6d00',GradientType=0 );
            padding: 15px;
            text-transform: uppercase;
            font-size: 20px;
            line-height: 1;
            color: #fff;
            border: none;
            border-radius: 2px;
            display: inline-block;
            text-align: center;
            font-weight: bold;

            opacity: 1;
            width: 100%;
        }
        .form-container > div + div {
            margin-top: 10px;
            color: black;
        intl-tel-input allow-dropdown separate-dial-code{width:100% !important}
        }
        .intl-tel-input {
            position: relative;
            display: inline-block;
            width: 100%;
        }
    </style>
    <main class="main">
        <div class="container">
            <h2 class="main__title ch1"><span class="txt00"></span>&nbsp;<span class="txt01"></span>&nbsp;<span class="txt02"></span>&nbsp;&nbsp;<span class="txt03" ></span></h2>
            <h3 class="main__sub-title ch2"><span class="txt10"></span>&nbsp;<span class="txt11"></span>&nbsp;<span class="txt12"></span>&nbsp;<span class="txt13"></span>&nbsp;<span class="txt14"></span>&nbsp;<span class="txt15"></span>&nbsp;<span class="txt16"></span></h3>

            <div class="registration-cont">
                <div class="video-cont">
                    <video src="/video/algorithm.mp4" class="algorithm-video" controls controlslist="nodownload"></video>
                </div>

                <div class="reg-form" id="reg-form">
                    <div class="reg-form__title ch3"><span class="txt20"></span>&nbsp;<span class="txt21"></span></div>

                    <div class="reg-form__preloader hidden">
                        <img src="/img/preloader2.gif" alt="" class="reg-form__preloader-img">
                    </div>
                    <?php $form = ActiveForm::begin(['action' => '/site/signup', 'id' => 'userPhoneForm', 'options' => ['class' => 'form-container', 'name' => 'application']]); ?>
                    <!--<form id="userPhoneForm" action="application.php" method="post" name="application" class="form-container">-->
                        <div class="form_group--firstname">
                            <?= $form->field($model, 'firstname')->input('text', ['name' => 'firstname', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '255', 'placeholder' => 'Имя *'])
                                ->label(false); ?>
                        </div>
                        <div class="form_group--lastname">
                            <?= $form->field($model, 'lastname')->input('text', ['name' => 'lastname', 'class' => 'input', 'minlength' => '2', 'maxlength' => '255', 'placeholder' => 'Фамилия'])
                                ->label(false); ?>
                        </div>
                        <div class="form_group--email">
                            <?= $form->field($model, 'email')->input('email', ['name' => 'email', 'class' => 'input require', 'required' => '', 'placeholder' => 'Email *'])
                                ->label(false); ?>
                        </div>
                        <div class="form_group--phone">
                            <?= $form->field($model, 'phone')->input('phone', ['name' => 'phone', 'class' => 'input require', 'required' => '', 'placeholder' => 'Номер телефона *'])
                                ->label(false); ?>
                            <?= Html::input('hidden', 'country_code', null, ['id' => 'country_code', 'name' => 'phone_code', 'class' => 'input require', 'required' => '']) ?>
                        </div>

                        <div class="checkbox-wrapper" style="color:white;"><label><input class="inverted valid" required="" name="conditions" type="checkbox" data-validation-type="conditions"  checked=""> Я согласен с <a href="/#" data-terms="use">Клиентским соглашением</a></label><div class="validation"><i class="fjs fjs-exclamation"></i></div> <div class="error"></div></div>
                        <div class="checkbox-wrapper"><label><input class="inverted valid" required="" name="age" type="checkbox" data-validation-type="age" checked=""> Мне больше 18 лет</label><div class="validation"><i class="fjs fjs-exclamation"></i></div> <div class="error"></div></div>
                        <button class="send-form" type="submit" onclick="validate()">ПРОДОЛЖИТЬ</button>
                    <?php ActiveForm::end(); ?>


                        <script type="text/javascript">
                            $("#signupform-phone").val(null);
                            $("#signupform-phone").intlTelInput({
                                initialCountry: "auto",
                                geoIpLookup: function(callback) {
                                    $.get('https://ipinfo.io?token=7d70a91745546b', function() {}, "jsonp").always(function(resp) {
                                        var countryCode = (resp && resp.country) ? resp.country : "";
                                        $('#country_code').val(countryCode);
                                        $('#kingdom').val(countryCode);
                                        $('#ip').val(resp.ip);
                                        callback(countryCode);
                                    });
                                },
                                utilsScript: "build/js/utils.js" // just for formatting/placeholders etc
                            });

                        </script>

                        <!--<script type="text/javascript">
                            $("form").submit(function() {
                                $("#phone2").val($("#phone").intlTelInput("getNumber"));
                            });
                        </script>-->

                        <!--<script type="text/javascript">
                            $.validator.addMethod("phoneNumValidation", function(value) {
                                return $("#phone").intlTelInput("isValidNumber")
                            }, 'Введите правильный номер');
                            var validate = function() {
                                $("#userPhoneForm").validate({
                                    rules: {
                                        phone: {
                                            required: true,
                                            phoneNumValidation: true
                                        }
                                    },
                                    messages: {
                                        phone: {
                                            required: "Введите номер"
                                        }
                                    },
                                    errorPlacement : function(error, element) {
                                        error.insertAfter($("#userPhoneDiv"));
                                    }
                                });
                            }

                            $(document).ready(function() {
                                $("#phone").intlTelInput({utilsScript: "build/js/utils.js"});
                            });

                        </script>-->






                        <div class="reg-form__icons">
                            <img src="/img/partners.png" alt="">
                        </div>
                </div>
            </div><!-- .registration-cont -->

            <div class="social-desc">
                <div class="social-desc__item">
                    <div class="social-desc__item-img-cont">
                        <img src="/img/whatsapp.png" alt="" class="social-desc__item-img">
                    </div>
                    <div class="social-desc__item-title">WhatsApp</div>

                    <p class="social-desc__item-desc">
                        WhatsApp — популярная бесплатная система мгновенного обмена текстовыми сообщениями для мобильных
                        и иных платформ с поддержкой голосовой и видеосвязи. Позволяет пересылать текстовые сообщения,
                        изображения, видео и аудио через Интернет. Клиент работает на платформах Android, iOS, Windows
                        Phone, Nokia Symbian, Nokia S40, а также ОС Windows и в виде веб-приложения.
                    </p>
                </div>

                <div class="social-desc__item">
                    <div class="social-desc__item-img-cont">
                        <img src="/img/viber.png" alt="" class="social-desc__item-img">
                    </div>
                    <div class="social-desc__item-title">Viber</div>

                    <p class="social-desc__item-desc">
                        Viber, «Вайбер»— приложение, которое позволяет совершать бесплатные звонки через сеть Wi-Fi или
                        мобильные сети (оплата только интернет-трафика по тарифу провайдера мобильной связи) между
                        пользователями с установленным Viber, а также передавать текстовые сообщения, изображения,
                        видео- и аудиосообщения, документы и файлы.
                    </p>
                </div>

                <div class="social-desc__item">
                    <div class="social-desc__item-img-cont">
                        <img src="/img/facebook.png" alt="" class="social-desc__item-img">
                    </div>
                    <div class="social-desc__item-title">Facebook Messenger</div>

                    <p class="social-desc__item-desc">
                        Facebook Messenger — приложение для обмена мгновенными сообщениями и видео, созданное Facebook.
                        Оно интегрировано с системой обмена сообщениями на основном сайте Facebook (Facebook Chat) и
                        построено на базе открытого протокола MQTT. По данным на апрель 2017 года месячная аудитория
                        мессенджера составляла 1 млрд. человек.
                    </p>
                </div>

                <div class="social-desc__item">
                    <div class="social-desc__item-img-cont">
                        <img src="/img/vkontakte.png" alt="" class="social-desc__item-img">
                    </div>
                    <div class="social-desc__item-title">Вконтакте</div>

                    <p class="social-desc__item-desc">
                        «ВКонтакте»— российская социальная сеть со штаб-квартирой в Санкт-Петербурге. Сайт доступен на
                        более чем 90 языках; особенно популярен среди русскоязычных пользователей. «ВКонтакте» позволяет
                        пользователям отправлять друг другу сообщения, создавать собственные страницы и сообщества,
                        обмениваться изображениями, тегами, аудио- и видеозаписями, играть в браузерные игры.
                    </p>
                </div>
            </div><!-- .social-desc -->

            <div class="text-center">
                <a href="/#reg-form" class="btn-custom btn-custom_blue ch4"><span class="txt30"></span>&nbsp;<span class="txt31"></span>&nbsp;<span class="txt32"></span>&nbsp;<span class="txt33"></span>
                </a>
                <br>
                <img src="/img/partners.png" alt="" class="partners-icons">
            </div>
        </div><!-- .container -->
    </main><!-- .main -->

    <footer class="footer">
        <div class="container">
            <div class="footer__cont">
                <div class="footer__links">
                    <div class="footer__copyright">©2018-2019 REDton</div>
                    <a href="/#desc_modal" data-terms="policy" class="footer__link">Политика конфиденциальности</a>
                    <a class="footer__email"></a>
                </div>
            </div>
        </div><!-- .container -->
    </footer><!-- .footer -->
</div><!-- .wrapper -->





<script src="/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/canva.min.js"></script>
<script>
    var txts = [
        ["0JrQkNCa", "0JfQkNCg0JDQkdCQ0KLQq9CS0JDQotCs", "0J3QkA==", "0JDQktCi0J7Qn9CY0JvQntCi0JU/"],
        ["0JjQndCd0J7QktCQ0KbQmNCe0J3QndCr0Jk=", "0JzQldCi0J7QlA==", "0JfQkNCg0JDQkdCe0KLQmtCQ", "0J7Qog==", "0KHQntCX0JTQkNCi0JXQm9CV0Jk=", "0JzQldCh0KHQldCd0JTQltCV0KDQkA==", "wqvQotCV0JvQldCT0KDQkNCcwrs="],
        ["0J/QntCU0JrQm9Cu0KfQmNCi0Kw=", "0J/QoNCe0JPQoNCQ0JzQnNCj"],
        ["0J/QntCU0JrQm9Cu0KfQmNCi0Kw=", "0J/QoNCe0JPQoNCQ0JzQnNCj", "0JjQndCd0J7QktCQ0KbQmNCe0J3QndCe0JPQng==", "0JfQkNCg0JDQkdCe0KLQmtCQ"]
    ]

    txts.forEach(function(mass,i) {
        mass.forEach(function(el,index){
            // console.log(stringToWords(el,i));
            $('.txt'+i+index).Canva({
                content: el,
                rtl: false,
                canvas: function(general,$canvas){
                    $canvas.css({
                        transform: 'translateY(8px)',
                        padding: '0 1px'
                    });

                },
            });
        })
    })
    function stringToWords(str,i) {
        var identifier = [];
        var enCodeStr = str.split(" ").map(function(elem,index){
            identifier.push('&nbsp;<span class="txt'+i+index+'"></span>')
            if(elem.length === 1) {
                return $.Canva.content(elem+" ").encode('base64');
            }
            else if(!elem) {
                return $.Canva.content(" ").encode('base64');
            }
            else {
                return $.Canva.content(elem).encode('base64');
            }
        })
        console.log(identifier.join(""));
        return enCodeStr;
    }
</script>

<script>
    $('#signupform-phone').one('input',function(){fbq('track', 'InitiateCheckout')}); fbq('track', 'ViewContent');
</script>