<div>
	<span style="margin-left: 39%; font-weight: 600;">
		Спасибо за регистрацию
	</span>
</div>
<?php $redirectUrl = Yii::$app->urlManager->createAbsoluteUrl(['/']) . 'site/signupapi?'
		. 'phone=' . $phone
		. '&email=' . Yii::$app->request->post('email')
		. '&firstname=' . Yii::$app->request->post('firstname')
		. '&lastname=' . Yii::$app->request->post('lastname')
		. '&country_code=' . Yii::$app->request->post('country_code');
?>
<script>
	setTimeout(function(){
		location.href = '<?= $redirectUrl ?>'; 
	}, 3000);
</script>
<!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1757135874386671');
            fbq('track', 'PageView');
        </script>
        <noscript>
        	<img height="1" width="1" style="display:none"
                       src="/https://www.facebook.com/tr?id=1757135874386671&ev=PageView&noscript=1"
         	/>
     	</noscript>
<!-- End Facebook Pixel Code -->
<style>
	body {
		background: #33FFE0;
	}
	span {
		color: white;
	}
</style>